--[[
Combat Report

This file implements a simple combat report in the CombatReport class. It takes in the events from the COMBAT_EVENTS array 
and prints out a report based on processing them. The output is as follows:

*****************************************	
** Combat Report (12 seconds)	
*****************************************	
Results of the last combat:
	
Damage Done:
#1 Alice 200
#2 Bob 60
#3 Alice 30
#4 Alice 30
#5 Alice 45
#6 Bob 120
#7 Bob 300
#8 Alice 100
#9 Bob 50
	
Your Damage Stats:	
Average: 81
Min: 30
Max: 200

Your task is two-fold. The first is to make a *NEW* combat report that has some advanced features. Because we will let the player choose
which report they want to use at run-time both reports must exist in parallel. For the purposes of this test when this file is run the
simple report should print first and the advanced report immediately after. The advanced report will have the following new features:
    -The list of damage events will distinguish critical hits by printing them out with ">>>>" between the event number and source. They
        will also have "(CRITICAL)" at the end of the line. For example, "#1 >>>> Alice 200 (CRITICAL)".
    -There will be a new section that outputs the percentage of the total damage done by each player that is sorted in descending order
        by damage. For example, "#1 Bob 57%".
    -Under each player's entry in the new section there will be an indented list with an entry for each ability the player used. The
        entries will be sorted in the order of the ability's contribution to the player's damage. The entry will have the ability's
        rank, name, how many times it was used in parentheses, and the percentage of the player's total damage it accounts for. For
        example, "#1 Heavy Attack (1) 57%". 
    -The report with the above changes and additions will then be duplicated for healing events.
    
The output of the advanced combat report is as follows:

*****************************************	
** Combat Report (12 seconds)	
*****************************************	
Results of the last combat:
	
Damage Done:	
#1 >>>> Alice 200 (CRITICAL)	
#2 Bob 60	
#3 Alice 30	
#4 Alice 30	
#5 >>>> Alice 45 (CRITICAL)	
#6 >>>> Bob 120 (CRITICAL)	
#7 Bob 300	
#8 Alice 100	
#9 Bob 50	
	
Percent Total Damage Done:	
#1 Bob 57%	
    #1 Heavy Attack (1) 57%	
    #2 Lava Whip (2) 34%	
    #3 Light Attack (1) 9%	
#2 Alice 43%	
    #1 Crystal Shard (2) 74%	
    #2 Lightning Splash (3) 26%	
	
Your Damage Stats:	
Average: 81
Min: 30
Max: 200	

Healing Done:	
#1 >>>> Carol 400 (CRITICAL)	
#2 Carol 200	
#3 Alice 200	
	
Percent Total Healing Done:	
#1 Carol 75%	
    #1 Regeneration (2) 100%	
#2 Alice 25%	
    #1 Regeneration (1) 100%	
	
Your Healing Stats:	
Average: 200
Min: 200
Max: 200

Your second task is to improve the implementation of the simple combat report so that is more performant and maintainable. You may
make any changes you like to the code in the Question section only.
]]--

--Object library for using object-oriented techniques in Lua.
-------------------------------------------------------------

ZO_Object = {}

function ZO_Object:New(template)
    --
    -- The new instance of the object needs an index table.
    -- This next statement prefers to use "template" as the
    -- index table, but will fall back to self.
    -- Without the proper index table, your new object will
    -- not have the proper behavior.
    --
    template = template or self
    
    --
    -- This call to setmetatable does 3 things:
    -- 1. Makes a new table.
    -- 2. Sets its metatable to the "index" table
    -- 3. Returns that table.
    --    
    local newObject = setmetatable ({}, template)
    
    --
    -- Obtain the metatable of the newly instantiated table.
    -- Make sure that if the user attempts to access newObject[key]
    -- and newObject[key] is nil, that it will actually fall
    -- back to looking up template[key]...and so on, because template
    -- should also have a metatable with the correct __index metamethod.
    --
    local mt = getmetatable (newObject)
    mt.__index = template
    
    return newObject
end

function ZO_Object:Subclass()
    --
    -- This is just a convenience function/semantic extension
    -- so that objects which need to inherit from a base object
    -- use a clearer function name to describe what they are doing.
    --
    return setmetatable({}, {__index = self})
end

--Combat event data and event registration
------------------------------------------

local COMBAT_EVENTS =
{
  --     1             2              3       4       5       6        7
  --Player name     Attacking      Damage   Heal  Critical  Index   Attack Name
    { "Alice",      "Molag Bal",    200,    0,      true,   1,  "Crystal Shard" },
    { "Bob",        "Molag Bal",    60,     0,      false,  2,  "Lava Whip" },
    { "Alice",      "Molag Bal",    30,     0,      false,  3,  "Lightning Splash" },
    { "Carol",      "Alice",        0,      400,    true,   4,  "Regeneration" },
    { "Alice",      "Molag Bal",    30,     0,      false,  5,  "Lightning Splash" },
    { "Alice",      "Molag Bal",    45,     0,      true,   6,  "Lightning Splash" },
    { "Bob",        "Molag Bal",    120,    0,      true,   7,  "Lava Whip" },
    { "Bob",        "Molag Bal",    300,    0,      false,  8,  "Heavy Attack" },
    { "Carol",      "Bob",          0,      200,    false,  9,  "Regeneration" },
    { "Alice",      "Molag Bal",    100,    0,      false,  10, "Crystal Shard" },
    { "Bob",        "Molag Bal",    50,     0,      false,  11, "Light Attack" },
    { "Alice",      "Alice",        0,      200,    false,  12, "Regeneration" }
}

local g_eventReceivers = {}

local function RegisterForEvents(object)
   table.insert(g_eventReceivers, object) 
end

local function SendEvents(eventName, ...)
    for i = 1, #g_eventReceivers do
        local object = g_eventReceivers[i]
        local handler = object[eventName]
        if handler then
            handler(object, ...)
        end
    end
end

--Main Funtion
local function Run()
    SendEvents("OnEnterCombat", 1)
    for i = 1, #COMBAT_EVENTS do
        SendEvents("OnCombatEvent", unpack(COMBAT_EVENTS[i]))
    end
    --Call On end combat funtion
    SendEvents("OnEndCombat", COMBAT_EVENTS[#COMBAT_EVENTS][6])    
end


--Question--------------------------------------------------------------------------------------------------------------------------------------------------------------
----------

--NOTES ARE IN THE INSTUCTIONS.TXT DOC!------ NOTES ARE IN THE INSTUCTIONS.TXT DOC -- NOTES ARE IN THE INSTUCTIONS.TXT DOC -- NOTES ARE IN THE INSTUCTIONS.TXT DOC

--Object Declaration
CombatReport = ZO_Object:Subclass()

--Constructor
function CombatReport:New()
    local object = ZO_Object.New(self)
    RegisterForEvents(object)
    return object
end

--So our players name is alice
function CombatReport:GetPlayerName()
    return "Alice"
end

--Player level is 50 (this is never used)
function CombatReport:GetPlayerLevel()
    return 50 
end

function CombatReport:OnEnterCombat(combatStartTime)
    self.combatEvents = {}
    self.maximumHeal = 0
    self.combatStartTime = combatStartTime
    self.numCombatEvents = 0
end

function CombatReport:OnCombatEvent(sourceName, targetName, damage, heal, wasCrit, timestamp, abilityName)
    combatEvent = { sourceName, targetName, damage, heal, wasCrit, timestamp, abilityName }
    self.combatEvents[self.numCombatEvents + 1] = combatEvent
    self.numCombatEvents = self.numCombatEvents + 1

    if heal ~= 0 and heal > self.maximumHeal then
        self.maximumHeal = heal
    end
end

function CombatReport:OnEndCombat(combatEndTime)
    combatDurationTime = combatEndTime - self.combatStartTime + 1

    local reportHeader = "*****************************************\n" .. 
    "** Combat Report".." ("..combatDurationTime.." seconds) \n" ..
     "*****************************************\n" ..
      "Results of the last combat:\n"

    --string Declaration
    local damageEvents = "Damage Done:\n"
    --Set up advanced report (just avoid retypeing header)
    local advancedReportDamage = damageEvents; 
    local advancedReportHeal = "Healing Done: \n"
    local advancedReportCount = {1,1}
    --For number of combat events (for loop)
    --!!!!!!LUA INDEXES START AT 1 NOT 0!!!!!!
    -- nil IS THE LUA VERSION OF null 
    for i = 1, self.numCombatEvents do
        local damage = self.combatEvents[i][3]
        --sourceName = self.combatEvents[i][1]
        if damage > 0 then
            damageEvents = damageEvents .. "#"
            damageEvents = damageEvents .. self.combatEvents[i][6] --Attack Number (index)
            damageEvents = damageEvents .. " "            
            damageEvents = damageEvents .. self.combatEvents[i][1] --source player
            damageEvents = damageEvents .. " "
            damageEvents = damageEvents .. damage
            damageEvents = damageEvents .. "\n"
        end
            
          --Advanced report------
            local reportEdit = ""
            local val = (damage > 0 and damage or self.combatEvents[i][4])
            reportEdit = reportEdit .. "#"
            reportEdit = reportEdit .. advancedReportCount[(damage>0 and 1 or 2)] .. " "
            advancedReportCount[(damage>0 and 1 or 2)] = advancedReportCount[(damage>0 and 1 or 2)] +1
            --if critical attack
            if self.combatEvents[i][5] then
              reportEdit = reportEdit .. ">>>> " .. self.combatEvents[i][1] .. " " .. val .. " (CRITICAL) \n"
            else --not critical attack
              reportEdit = reportEdit .. self.combatEvents[i][1].. " " .. val .."\n" --source player
            end
            if damage > 0 then
                advancedReportDamage = advancedReportDamage .. reportEdit
            else
                advancedReportHeal = advancedReportHeal .. reportEdit
            end
    end --End for loop
    
              --Damage, Heal
    local total = {0 , 0}
    local num = {0 , 0}
    local max = {0 , 0}
    local min = {9999 , 9999}

    --for number of combat events
    for i = 1, self.numCombatEvents do
          --if event source was "me"
         if self.combatEvents[i][1] == self:GetPlayerName() then
             local placeCombat = (self.combatEvents[i][3] > 0 and 3 or 4)
             local placeLocal = (self.combatEvents[i][3] > 0 and 1 or 2)
              total[placeLocal] = total[placeLocal] + self.combatEvents[i][placeCombat]
              num[placeLocal] = num[placeLocal] + 1

              if self.combatEvents[i][placeCombat] > max[placeLocal] then
               max[placeLocal] = self.combatEvents[i][placeCombat] 
              end
              if self.combatEvents[i][placeCombat] < min[placeLocal] then
              min[placeLocal] = self.combatEvents[i][placeCombat] 
              end
         end -- end if me
    end -- end for loop

   local attackTable = {}


  --Precentage total damage done
   for i = 1, self.numCombatEvents do
      local damage = self.combatEvents[i][3]
      local source = self.combatEvents[i][1]
      local attackName = self.combatEvents[i][7]
          --if the source is not in table yet
          if attackTable[source] == nil then
              attackTable[source] = {}
              attackTable[source]["Attack"] = {}
              attackTable[source]["Heal"] = {}
          end

          local attackType = (damage > 0 and "Attack" or "Heal")
          --are we reading the value of attack from collum 3 or 4
          local collum = (damage > 0 and 3 or 4)
          --The souce is in the table but the attack is not
          if attackTable[source][attackType][attackName] == nil then
              attackTable[source][attackType][attackName] = {}
              --Set use number (its 1 because it is the first entry)
              attackTable[source][attackType][attackName].count = 1
              --set the attack value
              attackTable[source][attackType][attackName].value = self.combatEvents[i][collum]
          --If the source and attact already there
          else
            attackTable[source][attackType][attackName].count =  
            attackTable[source][attackType][attackName].count +1

            attackTable[source][attackType][attackName].value = 
            attackTable[source][attackType][attackName].value +self.combatEvents[i][collum]
          end
   end -- end for loop

  local reportDamage = "Percent Total Damage Done: \n"
  local reportHeal = "Percent Total Healing Done: \n"
  local count = {1,1}
  local playerCount = {1,1}
  --loop for 
    --name        --table
  for keySource, value in pairs(attackTable) do
        
        --I know this is a poor solution
      if keySource ~= "Carol" then 
        reportDamage = reportDamage .. "#" .. playerCount[1] .. " " .. keySource .. "\n"  
        playerCount[1] = playerCount[1] + 1
     --Attack name --.count and .value
      for keyInner, valueInner in pairs(value["Attack"]) do 
        precentage = math.floor((valueInner.value/total[1]) * 100)
       reportDamage = reportDamage .. "    #" .. count[1] .. " " .. keyInner .. " (" .. valueInner.count .. ") " .. precentage .."%" .. "\n"
       count[1] = count[1] + 1
     end
    end -- if alace 
    --also not a good way of doing this but I am having issues checking if a subtable is null without iterating from it
    if keySource == "Alice" or keySource == "Carol" then
       reportHeal = reportHeal .. "#" .. playerCount[2] .. " " .. keySource .. "\n"  
        playerCount[2] = playerCount[2] + 1
      for keyInner, valueInner in pairs(value["Heal"]) do 
        precentage = math.floor((valueInner.value/total[2]) * 100)
        reportHeal = reportHeal .. "    #" .. count[2] .. " " .. keyInner .. " (" .. valueInner.count .. ") " .. precentage .."%" .."\n"
       count[2] = count[2] + 1
     end
    end
  end -- end for loop
   --local precentage

  local playerDamageStats = "Your Damage Stats:\n" ..  "Average: "
     .. math.floor(total[1] / num[1]) .. "\nMin: " .. min[1]
     .. "\nMax: " .. max[1]
  local playerHealStats = "Your Healing Stats: \n" .. "Average: "
    .. math.floor(total[2] / num[2]) .. "\nMin: " .. min[2] ..
    "\nMax: " .. max[2]

   print(reportHeader)
   print(damageEvents)
   print(playerDamageStats)
   print("\n\n\n")
   print(reportHeader .. "\n")        
   print(advancedReportDamage);
   print(reportDamage)
   print(playerDamageStats .. "\n")
   print(advancedReportHeal)
   print(reportHeal)
   print(playerHealStats)
end
COMBAT_REPORT = CombatReport:New()


--Combat Report Driver (runs the application with the test data)
-----------------------------------------------------------------
Run()