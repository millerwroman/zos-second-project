========================================================================
    LUA APPLICATION : Combat Report Application

	ZOS Lua Programmer Applicant Test
========================================================================

In this folder you will find a file named CombatReport.lua. At the top of that file is a comment explaining
the tasks to be completed. An easy way to test your code online is through the website: https://repl.it/languages/lua .
You are encouraged to include comments explaining your thought process as you rework and expand on the code.



Notes:
1. This was my first time in lua, so im sure there are some general practice things that I am unaware of

2. Removed Count from for loop as we have index already. No need for extra variable
3. Removed sourceName as we do not need to store the variable
4. Added Local keyword to local variables
5. Iterating though table so many times is slow, I would like to combine all loops into 2 if time permitted
6. While there must be a better way to check if an attack does not posses a null I am unaware of it (line 345,365)
7. Generally when working in a new language I would have asked for another eye on those minor things I was unsure how to do. however for the purpose if this eval I did not

